#Members:
#Estefanía Pérez Hidalgo.
#Luz Clara Mora Salazar.
#Fabián Alfaro González.

import sys
import requests
import json
#/Users/Paulina/Desktop/5.jpeg
import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont

subscription_key = None
   
def emotions(picture):
    """This function through the subscription of a key, we can send an image to Microsoft Azure
    and provide all the relevant data of the people scanned.""" 
    SUBSCRIPTION_KEY = 'cdafd7788a4f4e178bc85046917a1b79'
    BASE_URL = 'https://classes.cognitiveservices.azure.com/face/v1.0/'
    CF.BaseUrl.set(BASE_URL)
    CF.Key.set(SUBSCRIPTION_KEY)
    #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
    image_path = picture
    #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
                             BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()
    #print(analysis)
    return analysis
history_permanent=[]

def number_of_faces(lista):
    """Displays the number of faces in the scanned image."""
    num_faces = len(lista)
    return num_faces


def age_1(lista):
    """Shows the ages of the people in the scanned image."""
    ages = []
    """Browses through a list to find the age in the attributes' list to add them to a new list."""
    for face in lista:
        dic_faceAttributes = face['faceAttributes']
        age = dic_faceAttributes['age']
        if age >= 0:
            ages.append(age)
    return ages


def age_youngest(lista):
    """Shows the youngest person in the selected image."""
    ages_young = age_1(lista)
    cont_1 = 150
    """Goes through a list to check the people's age and compares them to catch the minor age among the list."""
    for x in ages_young:
        if x < cont_1:
            cont_1 = x
    return ('Ages =', ages_young, 'and the youngest is', int(cont_1), 'years old.')


def ages_and_average(lista):
    """Shows the average age of the image."""
    sum_1 = 0
    cont_2 = 0
    average = 0
    ages_aver = age_1(lista)
    """Goes through a list looking for the average age."""
    for x in ages_aver:
        if len(ages_aver) > cont_2:
            sum_1 = sum_1 + x
            cont_2 = cont_2 +1
    average = sum_1/cont_2
    return ("Ages =", ages_aver,"and the average age is", int(average), "years old.")


def face_ID_emotions(lista):
    """Displays the ID of the people and their emotions, then stores them in separate sublists."""
    list_1 = []
    """It scrolls through a list looking for each ID along with the emotion to store them in a new list, one by one."""
    for emot in lista:
        face_Id = emot['faceId']
        faceAttributes = emot['faceAttributes']
        emotion_1 = faceAttributes['emotion']
        temp = []
        temp.append(face_Id)
        temp.append(emotion_1)
        list_1.append(temp)
    return list_1


def age_gender_list_sublist(lista):
    """It shows the ages with its gender in a list ordered from highest to lowest."""
    age_gender_list=[]
    """Scroll through a list searchig for ages and genders to save them in a sublist."""
    for i in lista: 
        call2=i['faceAttributes']
        gender=call2['gender']
        age=call2['age']  
        list1=[age,gender]
        age_gender_list.append(list1)
        temp_list=[0]
        in_order_list=[]
        """Browses the sublist to accommodate the ages from oldest to youngest with their respective gender."""
    for i in age_gender_list:
        if i[0]>=temp_list[-1]:
            temp_list.append(i[0])
            in_order_list.append(i)
        else:
            posicion=0
            for n in in_order_list:
                if n[0]>=i[0]:
                    temp_list.insert(posicion,i[0])
                    in_order_list.insert(posicion,i)
                    break
                posicion+=1
            else:
                temp_list.insert(0,i[0])
                in_order_list.insert(0,i)             
    return in_order_list


def age_faceID_gender(lista):
    """It shows a list of faces and we call the function to match ages and genders."""
    age_gender_list=[]
    """Scrolls through a list looking for the ages, IDs and genders of the image to save them in a new list."""
    for i in lista:
        gender_list1=[]
        face_id=i['faceId']
        call2=i['faceAttributes']
        gender=call2['gender']
        age=call2['age']  
        gender_list1=[age,face_id,gender]
        age_gender_list.append(gender_list1)
    return age_gender_list

def age_alone(lista):
    """Function that stores only the ages."""
    age_list1=[]
    for i in lista:
        call2=i['faceAttributes']
        age=call2['age']  
        age_list1.append(age)
    return age_list1

def partition_lista(l):
    """Order numbers from lowest to highest."""
    minors = list()
    greater = list()
    pivote = l[-1]
    for x in l[:-1]:
        if x < pivote:
            minors.append(x)
        else:
            greater.append(x)
    return([minors, pivote, greater])

def ordenamiento_quickSort(lista):
    """The sorting function is created by quickSort, which return a sorted list."""
    if len(lista)>0:
        lista = partition_lista(lista)
        cont = 0
        while cont < len(lista):
            e = lista[cont]
            if type(e) == list:
                if len(e) == 0:
                    del lista[cont]
                elif len(e) == 1:
                    lista[cont] = e[0]
                else:
                    lista = lista[:cont]+partition_lista(e) + lista[cont+1:]
            else:
                cont = cont + 1
    return (lista)

def age_gender_QS(lista):
    """Displays a list sorted by the function quickSort, showing ages, genders and faces."""
    age_gender_face = age_faceID_gender(lista)
    call_age = age_alone(lista)
    age_sorted = ordenamiento_quickSort(call_age)
    final_list = []
    b = 0
    a = len(age_gender_face)
    while a == len(age_gender_face):
        for x in age_gender_face:
            age = x[0]
            c = age_sorted[b]
            if age == c:
                final_list.append(x)
                age_gender_face.remove(x)
                b = b+1
                a = len(age_gender_face)
                break
        if a == 0:
            break
    return final_list

def gender_female_male(lista):
    """It shows a list sorted by the insertion method, which prints genders, first females and then males."""
    p = age_gender_QS(lista)
    original_list_f = []
    original_list_m = []
    new_list_fm = []
    for r in p:
        r.pop(1)
        new_list_fm.append(r)
    la = len(new_list_fm)
    while la == len(new_list_fm):
        for fm in new_list_fm:
            if fm[1] == 'female':
                original_list_f.append(fm)
                new_list_fm.remove(fm)
                la = len(new_list_fm)
                break
            else:
                original_list_m.append(fm)
                new_list_fm.remove(fm)
                la = len(new_list_fm)
                break
        if la ==0:
            break
    tre = [original_list_f] + [original_list_m]
    return tre

def faceID_haircolor(lista):
    """Displays the people's ID and hair color of each person captured in the image."""
    """Scrolls through a list looking for people's ID together with their hair color and save them in a list. """
    for i in lista:
        face_id=i['faceId']
        call2=i['faceAttributes']
        in_hair=call2['hair']
        hair_color=in_hair['hairColor']
        temp=0
        for x in hair_color: 
            num=x['confidence']
            if num>temp:
                temp=num
                color=x['color']
                result = ''
                result == print(('The user with the ID:',face_id,'has',color,'hair.'))
    return result

def faces_accessories(lista):
    """Displays the IDs of the people who have or do not contain accessories in the image."""
    """Browses through the list of people's ID to find out what kind of accessories each person has."""
    for i in lista:
            list5= []
            face_id=i['faceId']
            call2=i['faceAttributes']
            access=call2['accessories']
            if access ==[]:
                print(face_id, 'The user has nothing.')
            else:
                for a in access:
                    y = a["type"]
                    list5.append(y)
                print(face_id, 'The user has', list5)

def show_image(picture, lista):
    """It shows the faces of the people in the image with a rectangle on the person's face."""
    lista1 = []
    for face in lista:
        fa = face['faceRectangle']
        lista1.append(fa)
    image = Image.open(picture)
    draw = ImageDraw.Draw(image)
    """It analyzes the position of the face in order to recreate the position of the reducer."""
    for fa in lista1:
        top = fa['top'] 
        left = fa['left']  
        width = fa['width'] 
        height = fa['height']
        draw.rectangle((left, top, left+width, top+ height), outline= "teal", width = 3)
    image.show(image)   

def high_percentage_happiness(lista):
    """It shows the ID of the person with the highest percentage of happiness in an average of people."""
    average_4 = 0.0
    """Browses the list for the IDs of the people with the highest emotions of happiness, to analyze them and see the highest average."""
    for i in lista:
        face_id=i['faceId']
        call2=i['faceAttributes']
        in_emotion=call2['emotion']
        for x, y in in_emotion.items():
            if x == 'happiness':
                if y > average_4:
                    average_4 = y
    return ("The person with the highest percentage of happiness is:", face_id, "with percentage of", average_4)

def female_blond(lista):
    """Shows the ID of the female with the blondest hair in the registered image."""
    total = 0
    """Scrolls through a list to find the ID of the female with the highest percentage of blonde hair in the image."""
    for face in lista:
        face_Id= face['faceId']
        dic_faceAttributes = face['faceAttributes']
        dic_gender = dic_faceAttributes['gender']
        hair = dic_faceAttributes['hair']
        haircolor = hair['hairColor']
        if dic_gender == 'female':
            for x in haircolor:
                if x['color'] == 'blond':
                    y1 = x['confidence']
                    if y1 > total:
                        total = y1
                        faceId = face_Id
            return ("The person who has the blondest hair and is female is", faceId,"and the percentage of blonde hair is:", total)
        else:
            return ("There is not a female person in the image.")                     
#C:\Users\Martin Perez\Downloads\c.jpg
def average_1(lista):
    """Shows the average age of the people in the registered image."""
    cont_3 = 0
    sum_2 = 0
    prom = 0
    """Scrolls through a list looking to analyze the average ages captured in the image."""
    for face in lista:
        dic_faceAttributes = face['faceAttributes']
        age = dic_faceAttributes['age']
        if age > 0:
            sum_2 = sum_2 + age
            cont_3 = cont_3 + 1
    prom = sum_2/cont_3
    return prom

def happy(lista):
    """It shows the ID of the person with the highest happiness emotion of the image, also in the inferior half of the average age."""
    total = 0
    x = []
    """It scans the list for the ID of the person who has the highest emotion of happiness and stores it in a list"""
    for face in lista:
        face_id = face['faceId']
        dic_faceAttributes = face['faceAttributes']
        emotion = dic_faceAttributes['emotion']
        age = dic_faceAttributes['age']
        t = average_1(lista)
        if age < t:    
            for x, y in emotion.items():
                if x == 'happiness':
                    happy = y
                    if happy > total:
                        total = happy
    x = [face_id, total]
    return ("The person with the highest percentage of happiness below the average age of the people in the image is:",x)

def groups_10(lista):
    """Displays a list with sublists to show the age and gender of the faces in the images, sorted by a range of 10."""
    tt = age_gender_list_sublist(lista)
    list_1 = []
    cont = 10
    h=0
    while h!=len(tt):
        temp = []
        for x in tt:
            age_1 = x[0]
            if age_1 < cont and age_1>=(cont-10):
                temp.append(x)
                h+=1
        if temp != []:
            list_1.append(temp)
        cont +=10
    return list_1

def more_smile(lista):
    """Shows the ID of the person with the biggest smile in the registered image."""
    smile_1=0
    """Scrolls through a list to find the ID that shows the best smile."""
    for i in lista:
        face_id=i['faceId']
        call2=i['faceAttributes']
        smile=call2['smile']
        if smile>smile_1:
            smile_1=smile
            list_t=[face_id]
    return list_t

def age_max(lista):
    """It shows the oldest person of all in the registered image."""
    age_list=0
    for i in lista:
        faceAttributes=i['faceAttributes']
        age=faceAttributes['age']
        if age>=age_list:
            age_list = age
    return age_list

def happiness_percentage(lista):
    """It shows the person who has the most prominent beard, and is the happiest in the registered image."""
    barba_1=0
    felicidad=0
    """Goes through a list searching for the person with the most prominent beard, and at the same time shows the highest emotion of happiness."""
    for u in lista:
        faceId=u['faceId']
        faceAttributes=u['faceAttributes']
        facialHair=faceAttributes['facialHair']
        barba=facialHair['beard']
        emotions=faceAttributes['emotion']
        happiness=emotions['happiness']
        if barba>=barba_1:
            if happiness>=felicidad:
                barba_1=barba
                felicidad=happiness
                list_T=[faceId]
    return list_T

def more_angry(lista):
    """It shows the angriest person with glasses in the recorded image."""
    enojado=0
    list_glasses=[]
    """Goes through the list looking for the person with the highest emotion of anger wearing glasses."""
    for u in lista:
        faceId=u['faceId']
        faceAttributes=u['faceAttributes']
        emotions=faceAttributes['emotion']
        angry=emotions['anger']
        lentes=faceAttributes['glasses']
        if lentes=="ReadingGlasses":
            if angry>=enojado:
                enojado=angry
                list_glasses=[faceId]
    if list_glasses==[]:
        print("No people with glasses.")
    else:
        print("The person with glasses and highest anger percentage is: ", list_glasses)

def history(lista):
    """It saves all the queries made, in order to carry out other more demanding functions based on the storage, and history of the queries."""
    for i in lista:
        history_diccionary={
        'faceId': '',
        'faceRectangle': {},
        'faceAttributes': {
            'gender': '', 
            'age': 0, 
            'emotion': {}, },
        'path':'',
            }
        call1=i['faceId']
        call2=i['faceRectangle']
        call3=i['faceAttributes']
        gender=call3['gender']
        age=call3['age']
        emotion=call3['emotion']
        path=pic
        history_diccionary['faceId']=call1
        history_diccionary['faceRectangle']=call2
        history_diccionary['faceAttributes']['gender']=gender
        history_diccionary['faceAttributes']['age']=age
        history_diccionary['faceAttributes']['emotion']=emotion
        history_diccionary['path']=path
        history_permanent.append(history_diccionary)
    return history_diccionary

def consults1(history_permanent):
    """It shows the youngest person with a rectangle on its face from all the images that were scanned."""
    ages_young = age_1(history_permanent)
    cont_1 = 150
    for x in ages_young:
        if x < cont_1:
    	    cont_1 = x
    youngest_age=cont_1
    for i in history_permanent:
        if i['faceAttributes']['age']==youngest_age:
                lista1 = list()
                fa = i['faceRectangle']
                lista1.append(fa)
                pic1=i['path']
                image = Image.open(pic1)
                draw = ImageDraw.Draw(image)
                for fa in lista1:
                    top = fa['top']
                    left = fa['left']  
                    width = fa['width'] 
                    height = fa['height']
                    draw.rectangle((left, top, left+width, top+ height), outline= "teal", width = 3)
        if i['faceAttributes']['age']==youngest_age:
            call1=i['faceAttributes']
            emotions_youngest=call1['emotion']
    return ("From the history of faces, the youngest face is ",youngest_age," years old. It's emotions are:",emotions_youngest, image.show(image))

def consults2(history_permanent):  
    """Shows the average of the emotions of all the registered images.""" 
    cont_3 = 0
    sum_2 = 0
    prom = 0
    for face in history_permanent:
        dic_faceAttributes = face['faceAttributes']
        emotions = dic_faceAttributes['emotion']
        for x, y in emotions.items():
            if y > 0:
                sum_2 = sum_2 + y
                cont_3 = cont_3 + 1
    prom = sum_2/cont_3
    return prom

def menu1():
    """The menu for the user to interact with the program."""
    while (True):
        print("Welcome to the menu.")
        print("\nType 30 for insert path.")
        print("\nType 31 to exit.")
        N = int(input("\nInsert the number: " ))
        if  N == 30:
            global picture
            picture = input("Insert the image's path: ")
            global pic
            pic=[]
            pic=picture
            global lista
            lista=[]
            lista = emotions(picture)
            history(lista)
        if N==31:
            break
        menu()

def menu():
    """Menu for the user's interaction with the program."""
    while (True):
        """This is the interaction menu in which the user executes the actions and requests for desired information.
        """
        print ("\n\tWelcome to the interaction menu.")
        print ("To see the number of faces type: 1.")
        print ("To see the youngest face type: 2.")
        print ("To see the average age type: 3.")
        print ("To see faceID and emotions type: 4.")
        print ("To see the age and gender of each face: type 5.")
        print ("To see the age and gender by quicksort order for each face: type 6.")
        print ("To see the gender of each face: type 7.")
        print ("To see faceID and hair color of each face: type 8.")
        print ("To see the accessory of each face: type 9.")
        print ("To see a rectangle on each face: type 10.")
        print ("To see the highest happiness percentage: type 11.")
        print ("To see famale with the blonest hair: type 12.")
        print ("To see the happiness face below the average: type 13.")
        print ("To see the faces' age and gender sorted by 10: type 14.")
        print ("To see the face with the widest smile: type 15.")
        print ("To see the oldest face: type 16.")
        print ("To see the happiest face with the most noticeable beard: type 17.")
        print ("To see the angriest person wearing glasses: type 18.")
        print ("To see insert a new path: type 19.")
        print ("To see the history: type 20.")
        print ("To see the requests1: type 21.")
        print ("To see the requests2: type 22.")
        print ("To finish, type 23.")
        cont=int(input("You have typed: "))

        if cont==0:
            return menu()
        if cont==1:
            print("\n\tIt's being shown: ")
            print("There are", (number_of_faces(lista)), "faces present in the image.")
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==2:
            print("\n\tIt's being shown: ")
            print(age_youngest(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==3:
            print("\n\tIt's being shown: ")
            print(ages_and_average(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==4:
            print("\n\tIt's being shown: ")
            print(face_ID_emotions(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==5:
            print("\n\tIt's being shown: ")
            print(age_gender_list_sublist(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==6:
            print("\n\tIt's being shown: ")
            print(age_gender_QS(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==7:
            print("\n\tIt's being shown: ")
            print(gender_female_male(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==8:
            print("\n\tIt's being shown: ")
            print(faceID_haircolor(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()   
        if cont==9:
            print("\n It's being shown: ")
            print(faces_accessories(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==10:
            print("\n\t It's being shown: ")
            show_image(picture,lista)
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==11:
            print("\n\tIt's being shown: ")
            print(high_percentage_happiness(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==12:
            print("\n\tIt's being shown: ")
            print(female_blond(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==13:
            print("\n\tIt's being shown: ")
            print(happy(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu() 
        if cont==14:
            print("\n\tIt's being shown: ")
            print(groups_10(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==15:
            print("\n\tIt's being shown: ")
            print("the person with the highest smile is:", more_smile(lista))  
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==16:
            print("\n\tIt's being shown: ")
            print("The oldest person is:", age_max(lista))  
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()       
        if cont==17:
            print("\n\t It's being shown: ")
            print("The person with highest beard and happiness percentage is:", happiness_percentage(lista))  
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==18:
            print("\n\tIt's being shown: ")
            print(more_angry(lista))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==19:
            print("\n\tIt's being shown: ")
            print(menu1())
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==20:
            print("\n\tThe history is: ")
            print(history_permanent)
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==21:
            print("\n\tThe history is: ")
            print(consults1(history_permanent))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==22:
            print("\n\tThe history is: ")
            print("the average of the emotions is",consults2(history_permanent))
            print("\n\tType a number to return to the menu: ")
            cont=int(input("You have typed: "))
            if cont>=0:
                return menu()
        if cont==23:
            print("Thank you very much for using our work/project.")
            break
        else:
            print("You have left the program because you typed a wrong number.")
            exit
menu1()